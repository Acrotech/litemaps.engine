﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Acrotech.LiteMaps.Engine.Storage;
using NLog;

namespace Acrotech.LiteMaps.Engine.Sources
{
    public abstract class TileImageSource
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public static readonly TileDownloader Downloader = new TileDownloader();

        protected TileImageSource(string key, string name, TileStorage storage)
        {
            Key = key;
            Name = name;
            Storage = storage;

            DownloadSet = new Dictionary<string, Tile>();
            DownloadQueue = new Queue<string>();
            WaitHandle = new ManualResetEventSlim(false);
        }

        private Dictionary<string, Tile> DownloadSet { get; set; }
        private Queue<string> DownloadQueue { get; set; }
        private ManualResetEventSlim WaitHandle { get; set; }

        public string Key { get; private set; }
        public string Name { get; private set; }

        public TileStorage Storage { get; private set; }

        public virtual int MinZoom { get { return 0; } }
        public virtual int MaxZoom { get { return 23; } }

        public virtual int TileSize { get { return 256; } }

        public virtual string ImageFormat { get { return "png"; } }

        public virtual bool SaveTileImage(Tile tile, byte[] content)
        {
            var saved = false;

            if (Storage != null)
            {
                saved = Storage.SaveTileImage(this, tile, content);
            }

            return saved;
        }

        public virtual Uri GetDownloadUri(Tile tile)
        {
            return null;
        }

        public static byte[] GetImageContent(Uri uri)
        {
            byte[] bytes = null;

            try
            {
                Logger.Debug("Downloading Image Content From {0}", uri.AbsoluteUri);

                var req = System.Net.WebRequest.Create(uri);

                using (var res = req.GetResponse())
                using (var stream = res.GetResponseStream())
                using (var br = new BinaryReader(stream))
                {
                    bytes = br.ReadBytes((int)res.ContentLength);
                }
            }
            catch (Exception e)
            {
                Logger.Warn("Tile Failed to Download", e);
            }

            return bytes;
        }

        public virtual Uri PrepareTile(Tile tile)
        {
            var uri = Storage.GetStorageUri(this, tile);

            if (uri == null)
            {
                QueueTileForDownload(tile);
            }

            return uri;
        }

        public virtual void QueueTileForDownload(Tile tile)
        {
            Downloader.Queue(this, tile);
        }

        public override string ToString()
        {
            return Key;
        }
    }
}
