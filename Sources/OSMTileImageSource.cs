﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Acrotech.LiteMaps.Engine.Storage;

namespace Acrotech.LiteMaps.Engine.Sources
{
    public class OSMTileImageSource : TileImageSource
    {
        public OSMTileImageSource(TileStorage storage)
            : base("OpenStreetMaps", "Open Street Maps", storage)
        {
        }

        public override Uri GetDownloadUri(Tile tile)
        {
            return new Uri(string.Format("http://{3}.tile.openstreetmap.org/{0}/{1}/{2}.png", tile.Zoom, tile.X, tile.Y, (char)(int)((tile.X + 2 * tile.Y) % 3 + 97)));
        }
    }
}
