﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;

namespace Acrotech.LiteMaps.Engine
{
    public struct Tile
    {
        private static readonly uint[] MapSizes = Enumerable.Range(0, 25).Select(x => (uint)256 << x).ToArray();

        public Tile(int x, int y, int zoom)
        {
            X = x;
            Y = y;
            Zoom = zoom;
        }

        public int X;
        public int Y;
        public int Zoom;

        public string Key { get { return ToQuadKey(); } }

        public string ToQuadKey()
        {
            var sb = new StringBuilder();

            var x = X;
            var y = Y;

            Normalize(ref x, ref Y, Zoom);

            for (int i = Zoom; i > 0; --i)
            {
                char digit = '0';
                int mask = 1 << (i - 1);

                if ((x & mask) != 0)
                {
                    digit++;
                }

                if ((y & mask) != 0)
                {
                    digit++;
                    digit++;
                }

                sb.Append(digit);
            }

            return sb.ToString();
        }

        public Tile Normalize()
        {
            return Normalize(this);
        }

        public static Tile FromQuadKey(string key)
        {
            var x = 0;
            var y = 0;
            var zoom = key.Length;

            for (int i = zoom; i > 0; --i)
            {
                int mask = 1 << (i - 1);

                switch (key[zoom - i])
                {
                    case '0':
                        break;
                    case '1':
                        x |= mask;
                        break;
                    case '2':
                        y |= mask;
                        break;
                    case '3':
                        x |= mask;
                        y |= mask;
                        break;
                    default:
                        throw new ArgumentException("Invalid QuadKey digit sequence.");
                }
            }

            return new Tile(x, y, zoom);
        }

        public static Tile FromPixel(int x, int y, int zoom, int tileSize)
        {
            return new Tile(x / tileSize, y / tileSize, zoom);
        }

        public static Tile Normalize(Tile tile)
        {
            var x = tile.X;
            var y = tile.Y;

            Normalize(ref x, ref y, tile.Zoom);

            return new Tile(x, y, tile.Zoom);
        }

        public static void Normalize(ref int x, ref int y, int zoom)
        {
            if (zoom <= 0)
            {
                x = 0;
                y = 0;
            }
            else
            {
                var maxCoord = 2 << (zoom - 1);

                x = (int)(x % maxCoord);
                y = (int)(y % maxCoord);

                if (x < 0)
                {
                    x = maxCoord + x;
                }
            }
        }

        public static uint GetMapSize(int zoom)
        {
            return MapSizes[zoom];
        }

        public override string ToString()
        {
            return string.Format("{0},{1} @ {2} ({3})", X, Y, Zoom, Key);
        }
    }
}
