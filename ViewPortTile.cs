﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Acrotech.LiteMaps.Engine
{
    public class ViewPortTile
    {
        public ViewPortTile(Tile tile, int x, int y)
        {
            Tile = tile;
            X = x;
            Y = y;
        }

        public Tile Tile { get; private set; }
        public int X { get; private set; }
        public int Y { get; private set; }

        public override string ToString()
        {
            return string.Format("{1}{0}{2}{0}({3}, {4})", Environment.NewLine, Tile, Tile.Normalize(), X, Y);
        }
    }
}
