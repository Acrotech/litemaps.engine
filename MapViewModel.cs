﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Acrotech.LiteMaps.Engine.Sources;
using Acrotech.LiteMaps.Engine.Storage;
using DotSpatial.Positioning;
using DotSpatial.Projections;
using NLog;

namespace Acrotech.LiteMaps.Engine
{
    public class MapViewModel : BaseViewModel
    {
        public MapViewModel()
        {
            TileImageSource = new OSMTileImageSource(LocalTileStorage.Default);
            ViewPort = new ViewPortViewModel(this);

            OnCenterPositionUpdated();
        }

        #region ZoomIn Command

        private DelegateCommand cmdZoomIn = null;
        public ICommand ZoomIn
        {
            get
            {
                if (cmdZoomIn == null)
                {
                    cmdZoomIn = new DelegateCommand(ExecuteZoomIn, CanZoomInExecute);
                }

                return cmdZoomIn;
            }
        }

        private bool CanZoomInExecute()
        {
            return ZoomLevel < TileImageSource.MaxZoom;
        }

        private void ExecuteZoomIn()
        {
            ++ZoomLevel;
        }

        private void RaiseCanZoomInExecuteChanged()
        {
            if (cmdZoomIn != null)
            {
                cmdZoomIn.RaiseCanExecuteChanged();
            }
        }

        #endregion

        #region ZoomOut Command

        private DelegateCommand cmdZoomOut = null;
        public ICommand ZoomOut
        {
            get
            {
                if (cmdZoomOut == null)
                {
                    cmdZoomOut = new DelegateCommand(ExecuteZoomOut, CanZoomOutExecute);
                }

                return cmdZoomOut;
            }
        }

        private bool CanZoomOutExecute()
        {
            return ZoomLevel > TileImageSource.MinZoom;
        }

        private void ExecuteZoomOut()
        {
            --ZoomLevel;
        }

        private void RaiseCanZoomOutExecuteChanged()
        {
            if (cmdZoomOut != null)
            {
                cmdZoomOut.RaiseCanExecuteChanged();
            }
        }

        #endregion

        #region CenterOnGps Command

        private DelegateCommand cmdCenterOnGps = null;
        public ICommand CenterOnGps
        {
            get
            {
                if (cmdCenterOnGps == null)
                {
                    cmdCenterOnGps = new DelegateCommand(ExecuteCenterOnGps, CanCenterOnGpsExecute);
                }

                return cmdCenterOnGps;
            }
        }

        private bool CanCenterOnGpsExecute()
        {
            return IsGpsCenterLocked == false && GpsPosition.IsEmpty == false && GpsPosition.IsInvalid == false;
        }

        private void ExecuteCenterOnGps()
        {
        }

        private void RaiseCanCenterOnGpsExecuteChanged()
        {
            if (cmdCenterOnGps != null)
            {
                cmdCenterOnGps.RaiseCanExecuteChanged();
            }
        }

        #endregion

        #region CenterOnTarget Command

        private DelegateCommand cmdCenterOnTarget = null;
        public ICommand CenterOnTarget
        {
            get
            {
                if (cmdCenterOnTarget == null)
                {
                    cmdCenterOnTarget = new DelegateCommand(ExecuteCenterOnTarget, CanCenterOnTargetExecute);
                }

                return cmdCenterOnTarget;
            }
        }

        private bool CanCenterOnTargetExecute()
        {
            return IsGpsCenterLocked == false;
        }

        private void ExecuteCenterOnTarget()
        {
        }

        private void RaiseCanCenterOnTargetExecuteChanged()
        {
            if (cmdCenterOnTarget != null)
            {
                cmdCenterOnTarget.RaiseCanExecuteChanged();
            }
        }

        #endregion

        #region ToggleUIVisibility Command

        private DelegateCommand cmdToggleUIVisibility = null;
        public ICommand ToggleUIVisibility
        {
            get
            {
                if (cmdToggleUIVisibility == null)
                {
                    cmdToggleUIVisibility = new DelegateCommand(ExecuteToggleUIVisibility, CanToggleUIVisibilityExecute);
                }

                return cmdToggleUIVisibility;
            }
        }

        private bool CanToggleUIVisibilityExecute()
        {
            return ZoomLevel < TileImageSource.MaxZoom;
        }

        private void ExecuteToggleUIVisibility()
        {
            IsUIVisible = !IsUIVisible;
        }

        private void RaiseCanToggleUIVisibilityExecuteChanged()
        {
            if (cmdToggleUIVisibility != null)
            {
                cmdToggleUIVisibility.RaiseCanExecuteChanged();
            }
        }

        #endregion

        #region TileImageSource

        private TileImageSource propTileImageSource = default(TileImageSource);
        public TileImageSource TileImageSource { get { return propTileImageSource; } set { RaiseAndSetIfChanged(ref propTileImageSource, value, "TileImageSource", () => { RaiseCanZoomInExecuteChanged(); RaiseCanZoomOutExecuteChanged(); }); } }

        #endregion

        #region GpsPosition

        private Position propGpsPosition = default(Position);
        public Position GpsPosition { get { return propGpsPosition; } set { RaiseAndSetIfChanged(ref propGpsPosition, value, "GpsPosition", () => { OnGpsPositionUpdated(); }); } }

        #endregion

        #region CenterPosition

        private Position propCenterPosition = default(Position);
        public Position CenterPosition { get { return propCenterPosition; } set { RaiseAndSetIfChanged(ref propCenterPosition, value, "CenterPosition", () => { OnCenterPositionUpdated(); }); } }

        #endregion

        #region IsGpsCenterLocked

        private bool propIsGpsCenterLocked = default(bool);
        public bool IsGpsCenterLocked { get { return propIsGpsCenterLocked; } set { RaiseAndSetIfChanged(ref propIsGpsCenterLocked, value, "IsGpsCenterLocked", () => { RaiseCanCenterOnGpsExecuteChanged(); }); } }

        #endregion

        #region IsTargetCenterLocked

        private bool propIsTargetCenterLocked = default(bool);
        public bool IsTargetCenterLocked { get { return propIsTargetCenterLocked; } set { RaiseAndSetIfChanged(ref propIsTargetCenterLocked, value, "IsTargetCenterLocked", () => { RaiseCanCenterOnTargetExecuteChanged(); }); } }

        #endregion

        #region ZoomLevel

        private int propZoomLevel = default(int);
        public int ZoomLevel { get { return propZoomLevel; } set { RaiseAndSetIfChanged(ref propZoomLevel, value, "ZoomLevel", () => { RaiseCanZoomInExecuteChanged(); RaiseCanZoomOutExecuteChanged(); ViewPort.Update(); }); } }

        #endregion

        #region IsUIVisible

        private bool propIsUIVisible = default(bool);
        public bool IsUIVisible { get { return propIsUIVisible; } set { RaiseAndSetIfChanged(ref propIsUIVisible, value, "IsUIVisible", () => { OnPropertyChanged("IsUIHidden"); }); } }

        #endregion

        #region TopLeftText

        private string propTopLeftText = default(string);
        public string TopLeftText { get { return propTopLeftText; } set { RaiseAndSetIfChanged(ref propTopLeftText, value, "TopLeftText", () => { OnPropertyChanged("IsTopLeftTextVisible"); }); } }

        #endregion

        #region TopRightText

        private string propTopRightText = default(string);
        public string TopRightText { get { return propTopRightText; } set { RaiseAndSetIfChanged(ref propTopRightText, value, "TopRightText", () => { OnPropertyChanged("IsTopRightTextVisible"); }); } }

        #endregion

        #region CenterPositionDetails

        private string propCenterPositionDetails = default(string);
        public string CenterPositionDetails { get { return propCenterPositionDetails; } set { RaiseAndSetIfChanged(ref propCenterPositionDetails, value, "CenterPositionDetails", () => { OnPropertyChanged("IsCurrentPositionDetailsVisible"); }); } }

        #endregion

        #region GpsPositionDetails

        private string propGpsPositionDetails = default(string);
        public string GpsPositionDetails { get { return propGpsPositionDetails; } set { RaiseAndSetIfChanged(ref propGpsPositionDetails, value, "GpsPositionDetails", () => { OnPropertyChanged("IsGpsPositionDetailsVisible"); }); } }

        #endregion

        public bool IsUIHidden { get { return !IsUIVisible; } set { IsUIVisible = !value; } }
        public bool IsTopLeftTextVisible { get { return string.IsNullOrEmpty(TopLeftText) == false; } }
        public bool IsTopRightTextVisible { get { return string.IsNullOrEmpty(TopRightText) == false; } }
        public bool IsCurrentPositionDetailsVisible { get { return string.IsNullOrEmpty(CenterPositionDetails) == false; } }
        public bool IsGpsPositionDetailsVisible { get { return string.IsNullOrEmpty(GpsPositionDetails) == false; } }

        public ViewPortViewModel ViewPort { get; private set; }

        private void OnCenterPositionUpdated()
        {
            CenterPositionDetails = string.Format("{0:d.ddddddi}", CenterPosition);

            ViewPort.Update();
        }

        private void OnGpsPositionUpdated()
        {
            GpsPositionDetails = string.Format("{0:d.ddddddi}", GpsPosition);

            if (IsGpsCenterLocked)
            {
                if (GpsPosition.IsEmpty == false && GpsPosition.IsInvalid == false)
                {
                    CenterPosition = GpsPosition;
                }
            }

            RaiseCanCenterOnGpsExecuteChanged();
        }
    }
}
