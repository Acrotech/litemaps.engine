﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Acrotech.LiteMaps.Engine.Sources;

namespace Acrotech.LiteMaps.Engine.Storage
{
    public abstract class TileStorage
    {
        public abstract bool SaveTileImage(TileImageSource source, Tile tile, byte[] content);
        public abstract Uri GetStorageUri(TileImageSource source, Tile tile);
    }
}
