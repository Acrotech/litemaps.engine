﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DotSpatial.Positioning;
using DotSpatial.Projections;

namespace Acrotech.LiteMaps.Engine
{
    public class ViewPortViewModel : BaseViewModel
    {
        public const double MinLatitude = -85.05112878;
        public const double MaxLatitude = 85.05112878;
        public const double MinLongitude = -180;
        public const double MaxLongitude = 180;
        public const double EarthRadiusKms = 6378137;

        public const double MaxWebMercX = 20037497.2108402;
        public const double MinWebMercX = -20037497.2108402;
        public const double MaxWebMercY = 20037508.3430388;
        public const double MinWebMercY = -20037508.3430388;
        public const double WebMercWidth = MaxWebMercX - MinWebMercX;
        public const double WebMercHeight = MaxWebMercY - MinWebMercY;

        public const double PiDiv180 = Math.PI / 180;
        public const double PiMul2EarthRadiusKms = Math.PI * 2 * EarthRadiusKms;

        private static readonly ProjectionInfo Wgs84Proj = KnownCoordinateSystems.Geographic.World.WGS1984;
        private static readonly ProjectionInfo MercWorldProj = KnownCoordinateSystems.Projected.World.Mercatorworld;
        private static readonly ProjectionInfo WebMercProj = KnownCoordinateSystems.Projected.World.WebMercator;

        private static readonly uint[] MapSizes = Enumerable.Range(0, 25).Select(x => (uint)256 << x).ToArray();

        public ViewPortViewModel(MapViewModel map)
        {
            Map = map;

            IsTileGridVisible = true;
            IsTileDebugTextVisible = true;
            IsCrossVisible = true;
        }

        #region Tiles

        private List<ViewPortTile> propTiles = default(List<ViewPortTile>);
        public List<ViewPortTile> Tiles { get { return propTiles; } set { RaiseAndSetIfChanged(ref propTiles, value, "Tiles", () => { }); } }

        #endregion

        #region IsTileGridVisible

        private bool propIsTileGridVisible = default(bool);
        public bool IsTileGridVisible { get { return propIsTileGridVisible; } set { RaiseAndSetIfChanged(ref propIsTileGridVisible, value, "IsTileGridVisible", () => { }); } }

        #endregion

        #region IsTileDebugTextVisible

        private bool propIsTileDebugTextVisible = default(bool);
        public bool IsTileDebugTextVisible { get { return propIsTileDebugTextVisible; } set { RaiseAndSetIfChanged(ref propIsTileDebugTextVisible, value, "IsTileDebugTextVisible", () => { }); } }

        #endregion

        #region IsCrossVisible

        private bool propIsCrossVisible = default(bool);
        public bool IsCrossVisible { get { return propIsCrossVisible; } set { RaiseAndSetIfChanged(ref propIsCrossVisible, value, "IsCrossVisible", () => { }); } }

        #endregion

        public MapViewModel Map { get; private set; }

        public int ViewPortWidth { get; private set; }
        public int ViewPortHeight { get; private set; }
        public int X { get; private set; }
        public int Y { get; private set; }

        public int CenterX { get { return X + ViewPortWidth / 2; } set { X = value - ViewPortWidth / 2; } }
        public int CenterY { get { return Y + ViewPortHeight / 2; } set { Y = value - ViewPortHeight / 2; } }

        public bool UpdateImages<T>(Dictionary<string, T> images, Func<Uri, T> creator)
        {
            var isUpdated = false;

            var tiles = Tiles.ToArray().Select(x => x.Tile.Key).Distinct().ToDictionary(x => x, x => Tile.FromQuadKey(x));

            foreach (var key in images.Keys.ToArray().Except(tiles.Keys))
            {
                Logger.Debug("Removing [{0}] From Cache", key);

                images.Remove(key);

                isUpdated = true;
            }

            foreach (var key in tiles.Keys.ToArray().Except(images.Keys))
            {
                var uri = Map.TileImageSource.PrepareTile(tiles[key]);

                if (uri != null)
                {
                    var image = creator(uri);

                    if (image != null)
                    {
                        Logger.Debug("Adding [{0}] From Cache", key);

                        images.Add(key, image);

                        isUpdated = true;
                    }
                }
            }

            return isUpdated;
        }

        public void Resize(int width, int height)
        {
            ViewPortWidth = width;
            ViewPortHeight = height;

            Update();
        }

        public void Pan(int dx, int dy)
        {
            Map.IsGpsCenterLocked = Map.IsTargetCenterLocked = false;

            Map.CenterPosition = GetPosition(CenterX + dx, CenterY + dy, Map.ZoomLevel);
        }

        public void Update()
        {
            if (ViewPortWidth > 0 && ViewPortHeight > 0)
            {
                var tileSize = Map.TileImageSource.TileSize;

                int x, y;
                
                GetPixelXY(Map.CenterPosition, Map.ZoomLevel, out x, out y);

                CenterX = x;
                CenterY = y;

                var tile = Tile.FromPixel((x - ViewPortWidth / 2), y - ViewPortHeight / 2, Map.ZoomLevel, tileSize);

                var dx = tile.X * tileSize - X;
                var dy = tile.Y * tileSize - Y;

                if (dx > 0)
                {
                    dx -= tileSize;
                }

                UpdateTiles(dx, dy);
            }
        }

        public void UpdateTiles(int dx, int dy)
        {
            var tiles = new List<ViewPortTile>();

            var mapSize = Tile.GetMapSize(Map.ZoomLevel);
            var tileSize = Map.TileImageSource.TileSize;

            for (var y = Y + dy; y < Y + ViewPortHeight && y < mapSize; y += tileSize)
            {
                if (y < 0)
                {
                    continue;
                }

                for (var x = X + dx; x < X + ViewPortWidth; x += tileSize)
                {
                    var tile = new Tile(x / tileSize, y / tileSize, Map.ZoomLevel);

                    tiles.Add(new ViewPortTile(tile, x - X, y - Y));
                }
            }

            Tiles = tiles;
        }

        public static void GetPixelXY(Position position, int zoom, out int x, out int y)
        {
            GetPixelXY(position.Latitude.DecimalDegrees, position.Longitude.DecimalDegrees, zoom, out x, out y);
        }

        public static void GetPixelXY(double lat, double lng, int zoom, out int x, out int y)
        {
            lat = lat.Clamp(MinLatitude, MaxLatitude);
            //lng = lng.Clamp(MinLongitude, MaxLongitude);

            var xy = new[] { lng, lat };
            Reproject.ReprojectPoints(xy, null, Wgs84Proj, WebMercProj, 0, 1);

            x = (int)((xy[0] - MinWebMercX) / WebMercWidth * Tile.GetMapSize(zoom));
            y = (int)((MaxWebMercY - xy[1]) / WebMercHeight * Tile.GetMapSize(zoom));
        }

        public static Position GetPosition(int x, int y, int zoom)
        {
            double lat, lng;

            GetPosition(x, y, zoom, out lat, out lng);

            return new Position(new Latitude(lat), new Longitude(lng));
        }

        public static void GetPosition(int x, int y, int zoom, out double lat, out double lng)
        {
            var mapSize = Tile.GetMapSize(zoom);

            x = (int)(x % mapSize);

            if (x < 0)
            {
                x = (int)(mapSize + x);
            }

            var xy = new[]
            {
                (double)(MinWebMercX + (double)x / mapSize * WebMercWidth),
                (double)(MaxWebMercY - y.Clamp(0, mapSize - 1) / mapSize * WebMercHeight)
            };

            Reproject.ReprojectPoints(xy, null, WebMercProj, Wgs84Proj, 0, 1);

            lat = xy[1];
            lng = xy[0];
        }
    }

    public static partial class ExtensionMethods
    {
        public static double Clamp(this double source, double min, double max)
        {
            return Math.Min(Math.Max(source, min), max);
        }

        public static double Clamp(this int source, double min, double max)
        {
            return Clamp((double)source, min, max);
        }
    }
}
