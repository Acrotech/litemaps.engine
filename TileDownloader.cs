﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Acrotech.LiteMaps.Engine.Sources;
using NLog;

namespace Acrotech.LiteMaps.Engine
{
    public class TileDownloader
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public event Action<TileImageSource, Tile> TileDownloaded = null;

        public TileDownloader()
        {
            DownloadQueue = new Stack<string>();
            DownloadSet = new Dictionary<string, TileDownload>();
            WaitHandle = new ManualResetEventSlim(false);

            ThreadPool.QueueUserWorkItem(_ => DownloadWorker());
        }

        private Stack<string> DownloadQueue { get; set; }
        private Dictionary<string, TileDownload> DownloadSet { get; set; }
        private ManualResetEventSlim WaitHandle { get; set; }

        public void Queue(TileImageSource source, Tile tile)
        {
            var tileDownload = new TileDownload(source, tile);
            var key = tileDownload.Key;

            if (DownloadSet.ContainsKey(key) == false)
            {
                DownloadSet.Add(key, tileDownload);
                DownloadQueue.Push(key);

                Logger.Debug("Queued {0} Tile for Download... {1}", source, tile);
            }
        }

        private void DownloadWorker()
        {
            while (WaitHandle.Wait(0) == false)
            {
                if (DownloadQueue.Any())
                {
                    var key = DownloadQueue.Pop();

                    var tileDownload = DownloadSet[key];

                    var uri = tileDownload.Source.GetDownloadUri(tileDownload.Tile);

                    if (uri != null)
                    {
                        Logger.Debug("Downloading {0}...", tileDownload);

                        var bytes = TileImageSource.GetImageContent(uri);

                        if (bytes != null && bytes.Any())
                        {
                            if (tileDownload.Source.SaveTileImage(tileDownload.Tile, bytes))
                            {
                                OnTileDownloaded(tileDownload.Source, tileDownload.Tile);

                                DownloadSet.Remove(key);
                            }
                        }
                    }
                }
                else
                {
                    WaitHandle.Wait(100);
                }
            }
        }

        protected virtual void OnTileDownloaded(TileImageSource source, Tile tile)
        {
            OnTileDownloaded(TileDownloaded, source, tile);
        }

        protected virtual void OnTileDownloaded(Action<TileImageSource, Tile> @event, TileImageSource source, Tile tile)
        {
            if (@event != null)
            {
                @event(source, tile);
            }
        }

        class TileDownload
        {
            public TileDownload(TileImageSource source, Tile tile)
            {
                Source = source;
                Tile = tile.Normalize();
            }

            public TileImageSource Source { get; private set; }
            public Tile Tile { get; private set; }

            public string Key { get { return string.Format("{0}_{1}", Source.Key, Tile.Key); } }

            public override string ToString()
            {
                return string.Format("{0} ({1})", Source, Tile);
            }
        }
    }
}
