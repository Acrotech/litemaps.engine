# README #

This project was developed as a simple light weight tile based mapping engine. I wanted to build a very simple to use but also very powerful and extensible portable engine that could be integrated into any project and provide access to tile based maps.

### What is this repository for? ###

I try to keep as much of the core functionality as possible inside this single engine library, all platform specific implementation will go into demo platform UI apps. This library will only supply the tools necessary to manage a tile based map, some UI code is requried to actually create and display the tiled images in the right location. This type of code ought to be very similar across most platforms, so it should be easy to port any UI implementation to another platform.

Note that the Engine currently functinos by storing image files to disk and then loading them from disk on demand. This behaviour can be modified by creating a custom TileImageSource class that overrides SaveImageFile and saves the images elsewhere.

### How do I get set up? ###

* Checkout the solution repository and demo one of the Desktop UI projects to see how everything works.
* Write your own TileImageSource
* Write your own UI implementation
* Integrate both into your own application

### Who do I talk to? ###

* Contact me if there are any issues or bugs
