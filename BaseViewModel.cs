﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using NLog;

namespace Acrotech.LiteMaps.Engine
{
    public abstract class BaseViewModel : INotifyPropertyChanged
    {
        protected readonly Logger Logger = null;

        public event PropertyChangedEventHandler PropertyChanged = null;

        protected BaseViewModel()
        {
            Logger = LogManager.GetLogger(GetType().Name);
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            OnPropertyChanged(PropertyChanged, propertyName);
        }

        protected virtual void OnPropertyChanged(PropertyChangedEventHandler handler, string propertyName)
        {
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public T RaiseAndSetIfChanged<T>(ref T backingField, T newValue, string propertyName, Action onChanged = null)
        {
            if (EqualityComparer<T>.Default.Equals(backingField, newValue) == false)
            {
                backingField = newValue;

                OnPropertyChanged(PropertyChanged, propertyName);

                if (onChanged != null)
                {
                    onChanged();
                }
            }

            return newValue;
        }
    }

    #region Commands

    public class DelegateCommand<T> : ICommand
    {
        public event EventHandler CanExecuteChanged = null;

        public DelegateCommand(Action<T> execute)
            : this(execute, null)
        {
        }

        public DelegateCommand(Action<T> execute, Predicate<T> canExecute)
        {
            ExecuteAction = execute;
            CanExecuteAction = canExecute;
        }

        private Predicate<T> CanExecuteAction { get; set; }
        private Action<T> ExecuteAction { get; set; }

        public virtual bool CanExecute(object parameter)
        {
            return (CanExecuteAction == null) ? true : CanExecuteAction((T)parameter);
        }

        public virtual void Execute(object parameter)
        {
            ExecuteAction((T)parameter);
        }

        public void RaiseCanExecuteChanged()
        {
            RaiseCanExecuteChanged(CanExecuteChanged);
        }

        protected virtual void RaiseCanExecuteChanged(EventHandler canExecuteChanged)
        {
            if (canExecuteChanged != null)
            {
                canExecuteChanged(this, EventArgs.Empty);
            }
        }
    }

    public class DelegateCommand : DelegateCommand<object>
    {
        public DelegateCommand(Action execute)
            : base(_ => execute(), null)
        {
        }

        public DelegateCommand(Action execute, Func<bool> canExecute)
            : base(_ => execute(), _ => canExecute())
        {
        }
    }

    public class CancelCommandArgs
    {
        public CancelCommandArgs(bool initialValue)
        {
            Cancel = initialValue;
        }

        public CancelCommandArgs()
            : this(false)
        {
        }

        public bool Cancel { get; set; }
    }

    #endregion

    public static partial class ExtensionMethods
    {
        public static bool CanExecute(this ICommand command)
        {
            return command.CanExecute(null);
        }

        public static void Execute(this ICommand command)
        {
            command.Execute(null);
        }
    }
}
